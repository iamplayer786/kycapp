package com.kyc.kycApp;

import java.util.List;

import com.kyc.kycApp.entity.Document;
import com.kyc.kycApp.repository.KycRepository;

public class App 
{
    public static void main(String[] args )
    {
        Document doc = new Document();
       
        doc.setFirstName("Abhi");
        doc.setLastName("Kumar");
        doc.setContactNumber(6784597345L);
        doc.setCity("Jamshedpur");
        doc.setState("Jharkhand");
        doc.setCountry("India");
        doc.setPincode(832101L);
        doc.setDocumentVerified(true);
        doc.setDocument("Adhar");
        
        KycRepository repository = new KycRepository();
        
//        For saving the data
       // repository.saveDocument(doc);
        
//        System.out.println(repository.findDocumentById(2L));
        
//        repository.deleteDocumentById(2L);
        
//        repository.updateContactNumberById(8271555876L,19L);
        
        repository.verifyDocumentById(1L);
        
//        List<Document> listall = repository.findAll();
//        listall.forEach(each->{
//        	System.out.println(each);
//        });
        
//        List<Document> listdoc = repository.findAllByDocumentType("Adhar");
//        listdoc.forEach(each->{
//        	System.out.println(each);
//        });
        
        
        
        
    }
}

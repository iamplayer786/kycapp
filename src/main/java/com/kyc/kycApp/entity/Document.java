package com.kyc.kycApp.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.kyc.kycApp.appconstant.Appconstant;

@Entity
@Table(name=Appconstant.KYC_DB)
public class Document implements Serializable{
	@Id
	@GenericGenerator(name = "m_auto", strategy = "increment")
	@GeneratedValue(generator= "m_auto")
	private Long id;
	
	@Column(name = "firstName")
	private String firstName;
	
	@Column(name = "lastName")
	private String lastName;
	
	@Column(name="contactNumber")
	private Long contactNumber;
	
	@Column(name = "city")
	private String city;
	
	@Column(name = "state")
	private String state;
	
	@Column(name = "country")
	private String country;
	
	@Column(name = "pincode")
	private Long pincode;
	
	@Column(name = "document")
	private String document;
	
	@Column(name = "document_verification")
	private boolean isDocumentVerified;
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Long getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(Long contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Long getPincode() {
		return pincode;
	}

	public void setPincode(Long pincode) {
		this.pincode = pincode;
	}

	public String getDocument() {
		return document;
	}

	public void setDocument(String document) {
		this.document = document;
	}

	public boolean isDocumentVerified() {
		return isDocumentVerified;
	}

	public void setDocumentVerified(boolean isDocumentVerified) {
		this.isDocumentVerified = isDocumentVerified;
	}
	
	public Document()
	{
		
	}

	@Override
	public String toString() {
		return "Document [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", contactNumber="
				+ contactNumber + ", city=" + city + ", state=" + state + ", country=" + country + ", pincode="
				+ pincode + ", document=" + document + ", isDocumentVerified=" + isDocumentVerified + "]";
	}
	
	
	
	
	
}

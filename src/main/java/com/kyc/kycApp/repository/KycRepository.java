package com.kyc.kycApp.repository;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import com.kyc.kycApp.entity.Document;
import com.kyc.kycApp.util.Utility;


public class KycRepository {
	
	public void saveDocument(Document k)
	{
		
		try 
		{
//			Configuration cfg = new Configuration();
//			cfg.configure();
//			SessionFactory sessionFactory = cfg.buildSessionFactory();
//			Session session = sessionFactory.openSession();
//			Transaction transaction = session.beginTransaction();
			
//			session.save(k);
//			transaction.commit();
			
			SessionFactory connection = Utility.getConnection();
			
			
			Session openSession = connection.openSession();
			//Session openSession = connection.openSession();
			Transaction transaction = openSession.beginTransaction();
			openSession.save(k);
			transaction.commit();
			
		} 
		catch(HibernateException e) {
			
		}
	}
	
	public Document findDocumentById(Long id)
	{
		SessionFactory connection = Utility.getConnection();
		Session session = connection.openSession();
		Document doc = session.get(Document.class, id);
		return doc;
	}
	
	public void deleteDocumentById(Long id)
	{
		SessionFactory connection = Utility.getConnection();
		Session session = connection.openSession();
		Document doc = session.get(Document.class, id);
		if(doc==null)System.out.println("Record Not Found");
		else
		{
			Transaction transaction = session.beginTransaction();
			session.delete(doc);
			transaction.commit();
			System.out.println("Data deleted Successfully");
		}
		
	}
	
	public void updateContactNumberById(Long number,Long id)
	{
		Document doc = findDocumentById(id);

		if(doc==null)System.out.println("Please enter valid ID");
		else
		{
			doc.setContactNumber(number);
			
			SessionFactory connection = Utility.getConnection();
			Session session = connection.openSession();
			Transaction transaction = session.beginTransaction();
			session.merge(doc);
			transaction.commit();
			System.out.println("Contact Number updated successfully");
		}
	}
	
	public void verifyDocumentById(Long id)
	{
		Document doc = findDocumentById(id);
		if(doc!=null)
		{
			if(doc.isDocumentVerified())
			{
				System.out.println("Document is Verified");
			}
			else 
			{
				SessionFactory connection = Utility.getConnection();
				Session session = connection.openSession();
				Transaction transaction = session.beginTransaction();
				session.merge(doc);
				transaction.commit();
				System.out.println("Document is verified successfully");
			}
		}
		else System.out.println("Please Enter valid ID");
	}
	
	public List<Document> findAll()
	{
		SessionFactory connection = Utility.getConnection();
		Session session = connection.openSession();
		String hql = "from Document";
		Query query = session.createQuery(hql);
		return query.list();
		
	}
	
	public List<Document> findAllByDocumentType(String docType)
	{
		SessionFactory connection = Utility.getConnection();		
		Session session = connection.openSession();
		String hql = "from Document where document=:doc";
		Query query = session.createQuery(hql);
		query.setParameter("doc", docType);
		return query.list();
	}
	public void updateCityAndPincodeByContactNumber(String city,String pinCode,Long cont)
	{
		SessionFactory connection = Utility.getConnection();		
		Session session = connection.openSession();
		Transaction transaction = session.beginTransaction();
		String hql = "update Document set city=:c,picode=:p where contactNumber=:n";
		Query query = session.createQuery(hql);
		query.setParameter("c",city);
		query.setParameter("p", pinCode);
		query.setParameter("n", cont);
		int rowsUpdate = query.executeUpdate();
		transaction.commit();
		if(rowsUpdate > 0)
		{
			System.out.println("Update successfull");
		}
		else
		{
			System.out.println("Unsuccessful");
		}
		
		
	}
	public void deletByFirstNameAndLastName(String last,String first)
	{

		SessionFactory connection = Utility.getConnection();
		Session session = connection.openSession();
		Transaction transaction = session.beginTransaction();
		String hql = "delete Document where firstName=:fn and lastName=:ln";
		Query query = session.createQuery(hql);
		query.setParameter("fn", first);
		query.setParameter("ln", last);
		int rowUpdate = query.executeUpdate();
		if(rowUpdate>0)System.out.println("delete sucessfully");
		else System.out.println("delete failed");
		transaction.commit();
	}
	
	
	
	
	
	
	
	

}

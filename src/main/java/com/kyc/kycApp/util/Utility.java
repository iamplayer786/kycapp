package com.kyc.kycApp.util;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class Utility {
	private static SessionFactory so = null;
	
	private Utility()
	{
		
	}
	public static SessionFactory getConnection()
	{
		if(so==null)
		{
//			Configuration cfg = new Configuration();
//			cfg.configure();
//			SessionFactory sessionFactory = cfg.buildSessionFactory();
//			so = sessionFactory;
			so = new Configuration().configure().buildSessionFactory();
		}
		return so;
		
	}

} 
